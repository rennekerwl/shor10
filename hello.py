
import os, zlib, datetime, re
from urlparse import urlparse
from sqlite3 import dbapi2 as sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, escape
import ipaddress

# create our little application :)
#This is a test
app = Flask(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'shortener.db'),
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


    
def connect_db():
    """Connects to the specific database.  returns: connection object"""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def init_db():
    """Initializes the database. returns: Nothing"""
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()


# @app.cli.command('initdb')
# def initdb_command():
#     """Creates the database tables."""
#     init_db()
#     print('Initialized the database.')


    """
    Opens a new database connection if there is none yet for the
    current application context.
    returns: Initialized database connection object
    """
def get_db():
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


    """
    Takes a base 10 number and turns it into base 62
    input: number
    returns: base62 string
    """
def int2base62(num):
    """Takes a base 10 number and turns it into base 62  Input: Integer  Returns: base62 string"""

    digs = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    if num < 0:
        sign = -1
    elif num == 0:
        return digs[0]
    else:
        sign = 1
    num *= sign
    digits = []
    while num:
      digits.append(digs[num % 62])
      num /= 62
    if sign < 0:
      digits.append('-')
    digits.reverse()
    return ''.join(digits)


    """
    Makes sure the URL is a valid URL
    input: url (string)
    returns: url(string) with 'http://'
    """
def validateURL(url):
    """Makes sure the URL is a valid URL  Input: url (string)  Returns: url(string) with http://  Sanitizes inputs."""
    o = escape(url)
    o = urlparse(o)
    if o.scheme == '':
        url = 'http://' + url
    return url


    """
    Takes a string as input and returns the hex value of the adler32 hash of
    Input: URL(string)
    Output: hashvalue in base 62
    """
def calculateHashVal(url):
    """Takes a string as input and returns the hex value of the adler32 hash of of the string as output"""
    urlHash = zlib.adler32(url)
    hashVal = int2base62(urlHash)
    return(hashVal)


    """
    gets list of URL in a dictionary format
    input: none
    returns: dictionary of hashes and urls
    """
def get_data():
    """Gets list of URLs in a dictionary format from the database Returns: dictionary of hashes and urls"""
    db = get_db()
    c = db.cursor()
    c.execute('select pk_id, url from entries order by pk_id desc')
    r = c.fetchall()
    print(r)
    r = dict(r)
    return(r)


def get_user_data():
    """This function returns the current user's id."""

    db = get_db()
    c = db.cursor()
    email  =  session['email']
    c.execute('Select pk_userid from users where email = ?', [email])
    r = c.fetchall()
    # print(r)
    #r = dict(r)
    return r[0][0]



def get_logged_in():
    """This function returns True if user is logged in.  Otherwise it returns False."""
    try: 
        return session["logged_in"]
    except:
        return False



def get_user_id():
    """This function returns the user's id.  Returns: id number of current user or -1 if none logged in"""
    try:
        if session['logged_in']:
            return str(get_user_data())
        else:
            return '-1'
    except:
        return " "



def get_user_email():
    """Retrieves user email from database  Returns: user email (string) or False if not logged in """

    try:
        if get_logged_in() != False:
            return session['email']
        else:
            return False
    except:
        return False


def passHash(password):
    """Takes a string as input and returns the hex value of the adler32 hash of string as output."""

    passwordHash = zlib.adler32(password)
    return(passwordHash)

def ip2int(ip):
	#convert to unicode
	#unicode = ip.encode('utf-8')
	this_is_unicode = unicode(ip)
	return int(ipaddress.ip_address(this_is_unicode))

def int2ip(int):
	return ipaddress.ip_address(int).__str__()

def ip2country(ip):
	"""Takes IP as a string and then returns the country that the IP belongs to as a string"""
	ip_int = str(ip2int(ip))
	
	db = get_db();
	cur = db.cursor()
        cur.execute("SELECT country FROM ip_range WHERE start_ip_int < ? AND ? < finish_ip_int", [ip_int, ip_int])
        response = cur.fetchone()
        country = response[0]

	return str(country)


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""

    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


@app.route('/')
def show_entries():
    """Defines shor10.com homepage"""
    url = None
    hash_val = None
    db = get_db()
    if get_logged_in() == True:
        query = "SELECT url, pk_id FROM entries WHERE fk_userid = '" + get_user_id() + "' ORDER BY creation_time DESC"
        cur = db.cursor()
        cur.execute(query)
        #cur = db.execute(query)
        shortenedLinks = cur.fetchall()
        entries = [{'url':link[0], 'hash_val':link[1]} for link in shortenedLinks]
        #Entries must be a list of dictionaries.
        #Each dictionary must have 'url' and 'hash_val' keys and values
        return render_template('show_entries.html', entries=entries)
    return render_template('show_entries.html')



@app.route('/add', methods=['POST'])
def add_entry():
    """Used to add links to database"""
   # if not session.get('logged_in'):
   #     abort(401)
    db = get_db()
    url = request.form['url']
    url = validateURL(url)
    userId = get_user_id()
    time = datetime.datetime.now()
    time.strftime("%Y-%m-%d %H:%M:%S")
    time = str(time)
    hash_val = calculateHashVal(url + userId + time)
    ip = request.remote_addr #gets user IP address
    db.execute('insert into entries (url, pk_id, fk_userid, num_clicks, click_ips, creation_time, creator_ip) values (?, ?, ?, ?, ?, ?,?)',
               [url, hash_val, userId, '0', ip, time, ip])
    db.commit()
    #message = "new entry was successfully posted for user " + userId
    message = "Your link has been shortened"
    flash(message)
    return render_template('show_entries.html', entries=[{'url':url, 'hash_val':hash_val}])



@app.route('/login', methods=['GET', 'POST'])
def login():
    """Code for the login page.  Redirects to homepage on success."""
    error = None
    if request.method == 'POST':  
       db = get_db()
       email = str(request.form['email'])
       email = escape(email)
       password = str(request.form['password'])
       passhash = str(passHash(password))
       
       match = False

       try:
           results = db.execute('select pswdhash from users where email = ?', [email])
           match  = str(results.fetchone()[0])##gets match if there is one

       except Exception,e:
           pass

       if match == passhash:
           '''commense with login'''
           session['logged_in'] = True
           session['email'] = email

           #######Log Login######
           ipaddress = request.remote_addr
           time = datetime.datetime.now()
           time.strftime("%Y-%m-%d %H:%M:%S")
           time = str(time)
           db = get_db()
           db.execute("INSERT INTO logins (ip_address, userid, time) values (?, ?, ?)", [ipaddress, get_user_data(), time])
           db.commit() 


           country = ip2country(ipaddress)
           message = "you were logged in " + session['email'] + " from country " + country
           flash(message)
           return redirect(url_for('show_entries'))
       elif match == False:
           flash('Invalid Login')
    return render_template('login.html', error=error)



@app.route('/register', methods=['GET', 'POST'])
def register():
    """Displays register page.  On success redirects to login page."""
    error = None
    db = get_db()

    if request.method == 'POST':
        email = request.form['email']
        email = escape(email)
        password = request.form['password']

        ##Check that password matches##
        if re.match(r"[^@]+@[^@]+\.[^@]+", email):
            ##match found
            ##Email already exists
            results = db.execute('select email from users where email = ?', [email])
            if results.fetchone() is not None:   
                return render_template('register.html', error="Email already exists")
            else:
                ##Add email to DB
                results = db.execute('insert into users (email, pswdhash) values (?, ?)', [email, passHash(password)])
                db.commit()
                flash("username created!")
                return render_template('login.html', error=error)
            
        else:
            error = "not a valid email"
            return render_template('register.html', error=error)

        

    return render_template('register.html', error=error)



@app.route('/logout')
def logout():
    """Logs out user.  Returns to homepage"""
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))


@app.route('/history')
def history():
    """Displays a user's history of shortened links"""
    db = get_db()
    if get_logged_in() == True:
        query = "SELECT url, pk_id, num_clicks FROM entries WHERE fk_userid = '" + get_user_id() + "' ORDER BY creation_time DESC"
        cur = db.cursor()
        cur.execute(query)
        #cur = db.execute(query)
        shortenedLinks = cur.fetchall()
        entries = [{'url':link[0], 'hash_val':link[1], 'num_clicks':link[2]} for link in shortenedLinks]
        #Entries must be a list of dictionaries.
        #Each dictionary must have 'url' and 'hash_val' keys and values
        return render_template('history.html', entries=entries)
    return redirect(url_for(('show_entries')))



@app.route('/invalid_url')
def display_error():
    """404 page"""
    return render_template('failure.html')



@app.route('/team')
def contact_us():
    """Contact info page"""
    return render_template('team.html')


@app.route('/<hashValue>')
def redirectTo(hashValue):
    """This is where the magic happens"""
    data = get_data()
    try:
        db = get_db()
        #update clicks and ip address of clicker
        ipaddress = "," + request.remote_addr
        db.execute("UPDATE entries SET num_clicks = num_clicks + 1 where pk_id = (?)", ([hashValue]))
        # db.execute("UPDATE entries SET click_ips = click_ips || ? where pk_id = ?", [ipaddress, hashValue])
        db.commit()
	db.execute("UPDATE entries SET click_ips = click_ips || ? where pk_id = ?", [ipaddress, hashValue]);
        db.commit()
        return redirect(data[hashValue])
    except KeyError:
        return redirect(url_for('display_error'))


@app.route('/map/<hashValue>')
def map(hashValue):
    # Get a list of ip addresses of clickees for the shor10 link
    db = get_db()
    cur = db.cursor()
    cur.execute("SELECT click_ips FROM entries WHERE pk_id = (?)", ([hashValue]))
    results = cur.fetchone()
    ips = results[0].split(',')

    # # Add a first country so the for loop doesn't break
    # firstCountry = ip2country(ips[0])
    # countries = [['firstCountry', 0]]
    countries = [['Country', 'Number of Clicks']]

    # For loops 
    for ip in ips[1:]:
        country = ip2country(ip)
        for sublist in countries:
            # Turn list of lists into a string and check if country name is in that string
            if country in sublist:
                sublist[-1] += 1
            elif country not in ''.join(str(r) for v in countries for r in v):
                countries.append([country, 0])
                
    return render_template('map.html', entries=countries)



if __name__ == '__main__':
    app.run('0.0.0.0', port=8080)

# from flask import Flask
# app = Flask(__name__)

# @app.route("/")
# def hello():
#     return "This seems to be working"

# if __name__ == "__main__":
#     app.run(host='0.0.0.0', port=8080)
