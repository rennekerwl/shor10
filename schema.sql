drop table if exists entries;
create table entries (
  pk_id text primary key not null,
  url text not null,
  fk_userid integer not null,
  num_clicks integer not null,
  click_ips text,
  last_click_time datetime,
  creation_time datetime,
  creator_ip text
);
drop table if exists users;
create table users (
  pk_userid integer primary key autoincrement not null,
  email text not null,
  pswdhash text not null
);
